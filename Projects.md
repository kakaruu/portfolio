# 주요 참가 프로젝트

[**:leftwards_arrow_with_hook: 메인 목차로 돌아가기 :leftwards_arrow_with_hook:**](./README.md)

## 주요 참가 프로젝트 목차

### [개인 프로젝트](#개인-프로젝트-상세)

- [2021.12.15 ~: 개인 홈페이지](#개인-홈페이지)
- [2021.02.26 ~: 개인 블로그](#개인-블로그)
- [2020.08.17 ~: Knowledge Stocks](#knowledge-stocks)
- [2020.04.24 ~ 2020.06.03: docker-sshd](#docker-sshd)
- [2016.01 ~ 2017.02: 석사 과정 졸업 프로젝트, 저널 게재](#석사-과정-졸업-프로젝트-저널-게재)

### [팀 프로젝트](#팀-프로젝트-상세)

- [2022.01.21 ~ 2022.02.14: 픽미업 - 지역 상권 쇼핑몰](#지역-상권-쇼핑몰-픽미업)
- [2022.02.18 ~ 2022.03.21: DOBDA - 주변 사람들과 의뢰를 주고받는 사이트](#의뢰-공유-사이트-dobda)

### [주요 사내 프로젝트](#주요-사내-프로젝트-상세)

- [2017.07 ~ 2020.07.03: 자동모니터링 시스템 개발 프로젝트](#자동모니터링-시스템-개발-프로젝트)
- [2017.06.02 ~ 2018.01.11: 마중물 과제](#마중물-과제)
- [2014.07 ~ 2015.02: GRAP(사내 SNS 시스템)](#grap)
- [2012.10.01 ~ 2013.12.10: 인천대학교 교내 소통 시스템](#인천대학교-교내-소통-시스템)

---

## 상세한 참가 프로젝트 소개

### 개인 프로젝트 상세

- #### 개인 홈페이지

  |수행기간|2021.12.15 ~|
  |:-:|-|
  |소속/직책|개인|
  |투입인원|개인|
  |주요업무|홈페이지 구현|
  |사용도구|**백엔드**<br>아직 없음<br><br>**프론트엔드**<br>Vue.js, Vuex, HTML, CSS, JS, VS Code<br><br>**DevOps**<br>GitLab CI/CD<br><br>**형상관리**<br>Git, git-flow, GitLab|

  - **상세 설명**

    옛날부터 저를 간단하게 소개할 수 있는 포트폴리오 홈페이지가 있었으면 좋겠다는 생각을 하고 있었지만, 방법을 몰라서 만들지 못하고 있었습니다.\
    풀스택 분야를 공부하면서 어느정도 기술 스택이 쌓이기 시작해서, Vue.js를 사용해서 홈페이지를 만들고, GitLab CI/CD를 활용해서 호스팅하고 있습니다.

  - **결과물**

    - [개인 홈페이지 링크](https://kakaruu.gitlab.io/)\
    ![개인 홈페이지](priv.postcoever/개인%20홈페이지.jpg)
    - [개인 홈페이지의 Git 저장소](https://gitlab.com/kakaruu/kakaruu.gitlab.io)
    - [JSP로 만들었던 홈페이지 초안의 Git 저장소](https://gitlab.com/kakaruu/jsp-portfolio)

---

- #### 개인 블로그

  |수행기간|2021.02.26 ~|
  |:-:|-|
  |소속/직책|개인|
  |투입인원|개인|
  |주요업무|블로그 운영|
  |사용도구|네이버 블로그|

  - **상세 설명**

    미뤄오던 개발 자료를 정리하고, 다른 개발자와의 교류를 하는 계기를 찾기 위해서 개발관련 내용을 중심으로 개인 블로그를 시작했습니다.\
    .NET 개발자 때부터 쌓아왔던 노하우나, 즐겨찾기에만 저장해놓았던 내용들을 정리해서 적기도하고, 웹 풀스택 개발자로 전향하면서 배우게 된 내용들을 나중에 참고하기 편하도록 기록하고 있습니다.

  - **결과물**

    - [개인 블로그 링크](https://blog.naver.com/jhlee21071)\
    ![개인 블로그](priv.postcoever/개인%20블로그.png)

---

- #### Knowledge Stocks

  |수행기간|2020.08.17 ~|
  |:-:|-|
  |소속/직책|개인|
  |투입인원|개인|
  |주요업무|유용한 팁이나, 지식을 쌓기 위한 나의 활동을 기록|
  |사용도구|Git, GitLab, 제가 접하고 학습한 모든 기술들|

  - **상세 설명**

    제가 기록하고 싶었던 유용한 팁이나, 지식을 쌓기 위해서 만들었던 프로젝트들을 기록하기 위한 저장소 입니다.\
    블로그와는 달리 소스 코드들을 저장하기 위한 장소입니다.

  - **결과물**
    - [GitLab 저장소](https://gitlab.com/knowledge_stocks)

---

- #### docker-sshd

  |수행기간|2020.04.24 ~ 2020.06.03|
  |:-:|-|
  |소속/직책|개인|
  |투입인원|개인|
  |주요업무|DockerFile 작성 및 이미지 배포|
  |사용도구|**관련 기술**<br>Docker, DinD, openssh-server, easy-rsa<br>**형상관리**<br>Git, git-flow, Github, Docker Hub|

  - **상세 설명**

    사용자가 이미 사용중인 PC에 기업체의 서비스를 직접 설치하게 되면 사용자의 Private Data에 기업체의 서비스가 접근할 수 있게 됩니다.\
    \
    서비스에 문제가 생겨서 `원격 지원`을 해야할 경우, 사용자의 PC에 직접 접속해야 하기 때문에 사용자의 개인 정보가 기업체에게 그대로 노출되고, 기업체의 실수로 사용자의 데이터에 손상을 입힐 가능성도 생기게 됩니다.\
    그리고, 사용자가 이용중인 다른 서비스와의 포트 충돌을 피하기 위해 초기 설정 또한 복잡해질 수 있습니다.\
    \
    이러한 문제를 방지하기 위해서 `DinD`를 활용하여 컨테이너들을 패키징 하는 방법을 생각하게 되었습니다.\
    기존의 Official Docker 이미지의 경우 SSH 관련 패키지가 설치되어 있지 않았기 때문에, 이를 상속받아서 필요한 패키지를 설치하고, 기본적인 rsa 키를 생성해주는 `docker-sshd` 이미지를 빌드해서 배포하고 있습니다.\
    \
    이렇게 만들어진 docker-sshd 이미지에 OpenVPN을 접목시켜서 사내의 프로젝트의 고객지원을 위해 사용하기도 했습니다.

  - **결과물**
    - [Docker hub 저장소](https://hub.docker.com/r/kakaruu/docker-sshd)
    - 코에버 근무 중 [서버 배포 관리](./ActivityHistory.md#서버-배포-관리)에서 활용
    - 코에버 근무 중 [VPN 서버 구축](./ActivityHistory.md#vpn-서버-구축)에서 활용

---

- #### 석사 과정 졸업 프로젝트, 저널 게재

  |수행기간|2016.01 ~ 2017.02 (1년 1개월)|
  |:-:|-|
  |소속/직책|인천대학교 모바일컴퓨팅 연구실/연구원|
  |투입인원|`석사 연구원 1명`|
  |주요업무|개인 학위 논문 주제 연구|
  |사용도구|**구현/실험**<br> eclipse, Arduino, RaspberryPi, MQTT, MQTT-SN, Zigbee, WiFi, 전력 측정기<br><br>**형상관리**<br> Git|

  - **상세 설명**

    연구실에서 주로 연구하고 있는 `무선 센서 네트워크 분야와, 이전 회사 근무 중 경험한 MQTT를 접목하여 센서 네트워크의 주요 이슈인 전력 소모 절감에 대하여 연구`하였습니다.\
    센서 네트워크를 구축을 위하여 `DigiMesh를 통한 MQTT-SN을 활용`하였습니다.\
    이 연구에서는 `주요 게이트웨이는 전력이 연결되어 있다고 가정하며, 하위 노드들의 스케줄링과 keep-alive 주기를 게이트웨이가 관리하는 방안`을 제안함으로써 하위 노드들의 충돌 회피와 전력 보존에서 유리할 수 있음을 실험적으로 증명하였습니다.\
  - **결과물**

    학위논문 `'전송 스케줄링과 가변 keep-alive 주기를 이용한 MQTT-SN의 전력효율 개선'`\
    과학 저널 `'Advanced Science Letters'`에 `'An Improvement of Energy Consumption Efficiency for MQTT-SN Using Transmission Scheduling and Variable Period of Keep-Alive'`라는 이름으로 논문 게재, [링크](https://www.researchgate.net/publication/322003087_An_Improvement_of_Energy_Consumption_Efficiency_for_MQTT-SN_Using_Transmission_Scheduling_and_Variable_Period_of_Keep-Alive)

  ---

### 팀 프로젝트 상세

- #### 지역 상권 쇼핑몰 - 픽미업

  |수행기간|2022.01.21 ~ 2022.02.14 (3주)|
  |:-:|-|
  |소속/직책|픽미업/팀장|
  |투입인원|`백엔드 3명`, 프론트 2명|
  |주요업무|백엔드 개발 및 프론트와 백엔드 연동, 프로젝트 전반 관리|
  |사용도구|**백엔드**<br>Spring boot, Spring Data JPA, Spring security, Java, Maven, IntelliJ<br><br>**프론트엔드**<br>React, React-redux, HTML, CSS, JS, VS Code<br><br>**DevOps**<br>GitLab CI/CD, Ubuntu 18.04, Docker, Naver Cloud Platform, MySQL<br><br>**형상관리**<br>Git, git-flow, GitLab|

  - **상세 설명**

    멀티캠퍼스의 '지능형 웹서비스 풀스택 개발' 과정의 전반적인 강의가 모두 끝난 뒤, 픽미업이라는 팀을 꾸리고 3주간의 프로젝트를 시작했습니다. 저는 유일한 전공자에다가 개발 경력이 있었기 때문에 팀장을 맡게 되었습니다. 저는 우선 각 팀원들이 개발해보고 싶은 기능들, 사용해보고 싶은 기술들을 먼저 알아보았습니다. 여기에서 WebRTC, JPA, 결제 API, Web Push, 위치 API 등 다양한 의견이 나왔습니다.\
    프로젝트의 주제를 쇼핑몰로 정한 이유는 이러한 기술들을 접목시켜보기에 적합하다고 판단했기 때문입니다.\
    <br>

    ```text
    개발을 다 완료하지 못하더라도 3주 동안 우리가 성장하는 것을 최우선 목표로 하자.
    ```

    기간내에 수행하기 어려운 주제라는 것을 알고 있었기에, 우리는 서로 성장하는 것을 최우선 목표로 잡았습니다.\
    \
    저는 git을 빼놓고는 협업을 진행하는 것이 사실상 어렵다고 판단하고 하루정도 [git의 사용법을 강의](https://gitlab.com/mulcam-semi-team2/hello-git)했습니다.\
    물론 하루만에 git과 완전히 친해질 수는 없었지만, 꾸준히 사용법을 알려주다보니, 점점 협업 구조가 잡혀가기 시작했습니다.\
    \
    저는 이 프로젝트에서 정말 많은 것을 배우고 성장할 수 있었습니다.\
    팀장이라는 위치에서 프로젝트를 잘 진행시키기 위해서 팀원들을 조율하는 일이 얼마나 어려운 일인지 경험하기도 했고, 백엔드와 프론트 개발을 겸하면서 프로젝트 시작할 당시에만 해도 어려웠었던 JPA나 React가 많이 친숙해지기도 했습니다.

  - **결과물**
    - [발표자료](https://docs.google.com/presentation/d/1mchniWW3BNz8kLZQ00RgmpcoqwAGtA4k/edit#slide=id.p1)
    - [프론트엔드](https://gitlab.com/mulcam-semi-team2/frontend/proto1.0)
    - [백엔드](https://gitlab.com/mulcam-semi-team2/backend/api-server)
    - [DB](https://gitlab.com/mulcam-semi-team2/backend/db)\
      ![우수상](team.pickmeup/우수상%20수상.jpg)

---

- #### 의뢰 공유 사이트 - DOBDA

  |수행기간|2022.02.18 ~ 2022.03.21 (4~5주)|
  |:-:|-|
  |소속/직책|DOBDA/팀장|
  |투입인원|백엔드 3명, `프론트 2명`|
  |주요업무|프론트 개발, 백엔드 개발, 프로젝트 관리|
  |사용도구|**백엔드**<br>Spring boot, Spring Data JPA, Spring security, Spring security messaging, Websocket STOMP, Java, Gradle, IntelliJ<br><br>**프론트엔드**<br>React, React-redux, HTML, CSS, JS, VS Code<br><br>**API**<br>오픈뱅킹, 카카오 지도, 카카오 우편번호, Naver SENS<br><br>**DevOps**<br>GitLab CI/CD, Ubuntu 18.04, Docker, Naver Cloud Platform, MySQL<br><br>**형상관리**<br>Git, git-flow, GitLab|

  - **상세 설명**

    멀티캠퍼스의 '지능형 웹서비스 풀스택 개발' 과정의 2차 프로젝트로, 다시 5명이 모여 DOBDA라는 팀을 꾸리고 4주간의 프로젝트를 시작했습니다.\
    \
    저는 두번째 팀에서도 유일한 전공자에다가 개발 경력이 있었기 때문에 팀장을 맡게 되었습니다.\
    저는 우선 각 팀원들이 개발해보고 싶은 기능들, 사용해보고 싶은 기술들을 먼저 알아보았지만, 의견이 잘 나오지 않았습니다.\
    그래서 제가 평소에 만들어 보고 싶었던 사용자 간의 의뢰 사이트를 아이디어로 프로젝트 주제를 잡게 되었습니다.\
    \
    저는 이전 프로젝트와 마찬가지로 git을 빼놓고는 협업을 진행하는 것이 사실상 어렵다고 판단하고 하루정도 [git의 사용법을 강의](https://gitlab.com/dobda/hello-git)했습니다.\
    이번에는 이전에 강의했던 내용에서 부족했던 부분을 보강하고 커리큘럼을 다시 짜고 난 뒤에 강의를 진행했습니다.\
    여전히 팀원분들이 git에 익숙해지는데는 어려움이 있었지만, 기초적인 이해도는 이전보다 높았던 것 같습니다.\
    \
    저는 이 프로젝트에서 UI 디자인을 제외하고 시스템의 설계부터 UX설계, 구현까지 전반적인 개발에 주도적으로 참가했습니다.\
    아직 초행자분들도 많았기에, 프로젝트 기간동안 기본적인 개발 방법들을 제가 팀원분들께 알려주는 형태로 진행되어서 사실상 대부분의 구현을 제가 수행하게 되었습니다.

  - **결과물**
    - [시연용 사이트](http://118.67.128.179:3000/)
    - [사이트 기능 설명 영상들](https://gitlab.com/dobda/frontend/dobda-front-react/-/blob/master/README.md)

---

### 주요 사내 프로젝트 상세

- ### 자동모니터링 시스템 개발 프로젝트

  |수행기간|2017.07 ~ 2020.07.03 (3년)|
  |:-:|-|
  |소속/직책|코에버정보기술사 연구소/PL,PE|
  |투입인원|`연구원 1명`, WPF 개발자 1명, 서버 개발자 1명, Data Scientist 1명, Winform 개발자 2~4명, PM 1명<br>코아칩스(센서), O-WELL(마케팅), 디자인허브(디자인)|
  |주요업무|모니터링 앱 PL/PE, 서버 PL/PE, DB PL/PE|
  |사용도구|**모니터링 앱**<br>Visual Studio 2015/2017/2019, WPF, C#, MVVM, Axis IP Camera, VAPIX API, FFmpeg, DevExpress, MQTT<br><br>**REST API 서버**<br>Visual Studio 2019, ASP.NET Core 3.1, C#, REST API, Entity Framework, MQTT<br><br>**DB**<br>MariaDB, UDF<br><br>**기타 서버 관련 도구**<br>Docker, Linux, Mosquitto, OpenVPN, ZYXEL, NodeJS, Mosca<br><br>**형상관리**<br>Git, git-flow, Submodule, GitLab|

  - **상세 설명**

    제조 공장의 `현장을 모니터링하기 위한 시스템을 개발하는 프로젝트`입니다.\
    처음에는 `저와 PM 한 분, 총 2명인 팀으로 시작`하였습니다.
    다양한 제조 업체와 거래하며 마케팅 노하우를 가지고 있는 일본의 `O-WELL` 사와, 센서를 전문 제작하고 있는 `코아칩스` 사와 함께 협력하였습니다.\
    저는 `일본어가 가능하여 O-WELL사를 통해 일본 고객사의 니즈를 수용하고 필요한 기능들을 시스템에 적용`하였습니다.\
    서버 등의 필수 분야의 개발 인력이 갖춰지지 않은 채로 프로젝트가 진행되어, `여러 방면으로 직접 나서서 일을 수행하다 보니 다양한 분야에 손을 뻗게 되었습니다.`\
    \
    이 프로젝트에서 수행한 일들을 하나의 항에 모두 나열하는 것은 번잡할 듯하여, `각각의 항을 따로 분리하여 작성해두었습니다.`\
    \
    **관련 항 목록**
    - [DMS 개발](./ActivityHistory.md#dynamic-monitoring-systemdms-개발)
    - [GitLab 관리](./ActivityHistory.md#gitlab-관리)
    - [Axis IP Camera 연동](./ActivityHistory.md#axis-ip-camera-연동)
    - [MariaDB 관리](./ActivityHistory.md#mariadb-관리)
    - [Rule 시스템 개발](./ActivityHistory.md#rule-시스템-개발)
    - [일본 파견 근무](./ActivityHistory.md#일본-파견-근무)
    - [서버 개발 환경 관리](./ActivityHistory.md#서버-개발-환경-관리)
    - [REST API 서버 개발](./ActivityHistory.md#rest-api-서버-개발)
    - [MQTT 푸시 서버 구축](./ActivityHistory.md#mqtt-푸시-서버-구축)
    - [서버 배포 관리](./ActivityHistory.md#서버-배포-관리)
    - [고객 지원용 VPN 서버 구축](./ActivityHistory.md#vpn-서버-구축)

---

- #### 마중물 과제

  |수행기간|2017.06.02 ~ 2018.01.11 (7개월)|
  |:-:|-|
  |소속/직책|코에버정보기술사 연구소/PL|
  |투입인원|`연구원 1명`<br>레티그리드(센서)|
  |주요업무|모니터링 프로그램과 하이브리드 서버 구현, 정량적 목표 달성, 정산 및 보고서 작성|
  |사용도구|**데이터 모니터링 프로그램**<br>Visual Studio 2015, Winform, C#<br><br>**하이브리드 서버**<br>Linux, RaspberryPi, C++, Mono, ASP.NET, MSSQL, MAC Switching, ARP, GARP<br><br>**전력 센서와의 통신**<br>Modbus TCP<br><br>**형상관리**<br>TFS|

  - **상세 설명**

    4년간 진행하는 국책과제로, 입사 후 바로 담당하게 된 과제입니다.\
    담당했을 당시, 마지막 연차 6개월을 남겨둔 채로 과제의 핵심인 하이브리드 서버가 준비되어 있지 않은 상태였으며, 정량적 목표도 모두 미달인 상태였습니다.\
    우선 `Modbus TCP와 MSSQL를 사용하여 참가기관의 센서와 통신하고 데이터를 저장`할 수 있도록 하였습니다.\
    그 뒤 `고사양의 서버와 저사양의 서버가 트래픽 수준에 따라 핸드오프 하는 하이브리드 서버를 구현`했습니다.\
    `정량적 목표를 달성하기 위하여 논문을 작성하고 인증기관의 테스트`를 거쳤으며, `완료 보고까지 진행하며 과제를 마무리`하였습니다.
    이 과제에서 연구한 내용으로 작성한 논문은 `CEIC2017에서 우수논문상을 수상`하기도 했습니다.

  - **결과물**

    과제 완료 평가까지 통과시키고 회사의 신임을 얻을 수 있었습니다.\
    \
    인증기관 테스트 결과\
    ![마중물 테스트 결과](corp.coever/마중물.png)\
    ![마중물 테스트 결과](corp.coever/마중물_2.png)\
    \
    CEIC2017 우수논문상 수상\
    ![CEIC2017 우수논문상](corp.coever/마중물_3.png)

---

- #### Grap

  |수행기간|2014.07 ~ 2015.02 (7개월)|
  |:-:|-|
  |소속/직책|파트너사 개발팀/팀원|
  |투입인원|`안드로이드 개발자 1명`, IOS 개발자 1명, 서버 개발자 1명, 디자이너 1명|
  |주요업무|안드로이드 앱 개발 전담|
  |사용도구|**안드로이드 앱 개발**<br> eclipse, Java, Android SDK, GCM, MQTT, SQLite<br><br>**형상관리**<br> Git, Bitbucket|

  - **상세 설명**

    `비즈니스용 SNS 시스템`을 개발하는 자체 프로젝트입니다.\
    초기의 컨셉샷이 `㈜신세계I&C`에서 좋은 반응을 얻었고, 이후 투자를 받으며 진행된 프로젝트입니다.\
    `채팅, 사원 관리, 그룹 기능의 기틀을 구현`하고 `2차 시연 테스트까지 진행 후 퇴사`하였습니다.
  - **결과물**\
    [Grap 사이트 (현재 서비스 종료. 카카오워크에 병합)](https://www.grap.io/ko/intro)\
    \
    제가 개발할 당시의 앱의 모습\
    ![그랩 어플리케이션의 이미지](corp.partner/Grap.png)

---

- #### 인천대학교 교내 소통 시스템

  |수행기간|2012.10.01 ~ 2013.12.10 (1년 2개월)|
  |:-:|-|
  |소속/직책|파트너사 개발팀/팀원|
  |투입인원|`앱 개발자 1명`, 서버 개발자 1명|
  |주요업무|모바일 앱 개발 전담, Feed 서버 개발|
  |사용도구|**안드로이드 앱 개발**<br> eclipse, Java, Android SDK, GCM, SQLite<br><br>**IOS 앱 개발**<br> Xcode, Objective-C, APNS<br><br>**Feed 서버 개발**<br> JSP, RSS<br><br>**형상관리**<br> SVN|

  - **상세 설명**

    인천대학교의 문자 비용 부담을 줄이기 위해 시작된 프로젝트입니다.<br>
    `교내 공지사항 알림, 직원 간의 채팅, 같은 강의를 듣는 학생들 간의 정보 공유, 강의 시간표` 등을 제공하였습니다.<br>
    처음으로 실무 개발을 경험하게 해 준 프로젝트입니다.
  - **결과물**

    ![인천대학교 알리미](corp.partner/인천대학교%20교내%20소통%20시스템.png)
